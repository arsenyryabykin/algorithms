/* Сортировка обменом (пузырьковая), O(n^2) */

#include <iostream>
#include <vector>
#include <algorithm>

void bubble_sort(int* array, int n)
{
    int length = n, max_index;
    // Цикл по массиву для совершения (n-1)-го прохода
    for(int j = 0; j != n - 1; j++)
    {
        // Цикл для обмена эл-тов
        for(int i = 0; i < length - 1; i++)
        {
            // Если эл-т больше следующего, если нет, переходим к след. эл-ту
            if(array[i] > array[i + 1])
            {
                std::swap(array[i], array[i + 1]);  // Обмен неотсортированных рядом стоящих эл-в
                max_index = i + 1;
            }
        }
        length = max_index;
    }
}

void bubble_sort_advanced(int* array, int n)
{
    int length = n, max_index;
    bool exit = false;
    while(!exit)
    {
        exit = true;
        for(int i = 0; i < length - 1; i++)
        {
            if(array[i] > array[i + 1])
            {
                std::swap(array[i], array[i + 1]);
                max_index = i + 1;
                exit = false;
            }
        }
        length = max_index;
    }
}

int main()
{
    const int size = 10;
    int array[size] = {3, 0, 6, 12, 1, 11, 9, 11, 0, 3};

    for(int p = 0; p < size; p++)
        std::cout << array[p] << " ";
    std::cout << std::endl << std::endl;
    
    bubble_sort(array, size);
    
    for(int p = 0; p < size; p++)
        std::cout << array[p] << " ";
    std::cout << std::endl;

    return 0;
}
