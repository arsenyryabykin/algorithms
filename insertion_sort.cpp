/* Сортировка вставкой, O(n^2)*
   Подходит, если массив изначально "почти" отсортирован */
#include <iostream>

void insertion_sort(int* array, int n)
{
    int tmp;    // Переменная для сохр-я i-го эл-та из неотсортированной части массива
    // В кач. отсортированной части массива принимаем один первый эл-т
    for(int i = 1; i < n; i++)
    {
        tmp = array[i]; // Сохраняем значение i-го эл-та
        // Цикл слева направо по отсортированной части массива для поиска места вставки
        for(int j = 0; j < i; j++)
        {
            if(array[j] < tmp)  // Если эл-т меньше вставляемого, идем дальше
                continue;
            if(array[j] > tmp)  
            {
                // Если эл-т больше вставляемого, осуществляем сдвиг эл-в от j-го до (i-1)-го
                for(int k = i; k > j; k--)
                {
                    array[k] = array[k - 1];
                }
                // На освободившееся место вставляем эл-т
                array[j] = tmp;
                break;  // Переход к следующему эл-ту в неотсортированной части
            }
        }
   }
}

/* В данном варианте проход по отсортированной части массива для поиска места вставки
   осуществляется не слева направо от 0-го до (i-1)-го эл-та. В отсортированной части
   выбирается средний элемент и сравнивается с i-м эл-том. Если i-ый эл-т больше среднего,
   поиск продолжается от среднего эл-та вправо, иначе - от 0-го эл-та до среднего */
void insertion_sort_advanced(int* array, int n)
{
    /* tmp - Переменная для сохр-я i-го эл-та из неотсортированной части массива;
       middle - Индекс среднего эл-та в отсортированной части;
       start, finish - Индексы начала и конца поиска места вставки в отсортированной части; */
    int tmp, middle, start, finish;
    // В кач. отсортированной части массива принимаем один первый эл-т
    for(int i = 1; i < n; i++)
    {
        tmp = array[i];     // Сохраняем значение i-го эл-та
        middle = (i - 1)/2; // Индекс среднего эл-та (i - 1 + 0)/2
        
        // Если i-й эл-т меньше среднего, идем от по отсортированной части от 0 до среднего эл-та вкл.
        if(tmp < array[middle])
        {
            start = 0;
            finish = middle;
        }
        // Если i-й эл-т больше среднего, идем от среднего эл-та до (i-1)-го вкл.
        if(tmp >= array[middle])
        {
            start = middle;
            finish = i - 1;
        }
        // Цикл по выбранному куску отсортированной части массива для поиска места вставки
        for(int j = start; j <= finish; j++)
        {
            if(array[j] < tmp)  // Если эл-т меньше вставляемого, идем дальше
                continue;
            if(array[j] > tmp)
            {
                // Если эл-т больше вставляемого, осуществляем сдвиг эл-в от j-го до (i-1)-го
                for(int k = i; k > j; k--)
                {
                    array[k] = array[k - 1];
                }
                // На освободившееся место вставляем эл-т
                array[j] = tmp;
                break;  // Переход к следующему эл-ту в неотсортированной части
            }
        }
  }
}

int main()
{
    const int size = 10;
    int array[size] = {7, 12, 11, 8, 12, 6, 10, 9, 8, 5};

    for(int p = 0; p < size; p++)
        std::cout << array[p] << " ";
    std::cout << std::endl;
    
    insertion_sort_advanced(array, size);
    
    for(int p = 0; p < size; p++)
        std::cout << array[p] << " ";
    std::cout << std::endl;
    
    return 0;
}