/* Быстрая сортировка Хоара O(n*log2(n)) 
   Реализация через несколько функций*/
#include <algorithm>
#include <iostream>

// Возвращает позицию эл-та м-у подмассивами, требующими рекурсивной сортировки
int partition(int* array, int first, int last) 
{
    int l = first, r = last;        // Ставим указатели на крайние эл-ты подмассива
    int pivot = array[(l + r)/2];   // Берем опорное значение
    // Пока указатели не встретятся и не разойдутся, продолжать сортировать эл-ты
    while(l <= r)
    {
        while(array[l] < pivot) // Если эл-т меньше опорного, идем дальше направо
            l++;
        while(array[r] > pivot) // Если эл-т больше опорного, идем дальше налево
            r--;
       // Когда нашлась пара неправильно стоящих эл-тов (или равных опорному), производим обмен
        // Сдвигаем указатели дальше
        if(l <= r)
        {
            std::swap(array[l], array[r]);
            l++;
            r--;
        }
    }
    return l;
}

void quick_sort(int* array, int first, int last)
{
    if(first == last)   // Если в подмассиве 1 элемент, пропускаем, он уже отсортирован
        return;
    int rightStart = partition(array, first, last); // Получаем индекс эл-та
    quick_sort(array, first, rightStart - 1);       // Сортируем левый подмассив
    quick_sort(array, rightStart, last);            // Сортируем правый подмассив
}

void quick_sort(int* array, int n)
{
    quick_sort(array, 0, n - 1);
}


int main()
{
    const int size = 10;
    int array[size] = {3, 0, 6, 12, 1, 11, 9, 11, 0, 3};
    for(int p = 0; p < size; p++)
        std::cout << array[p] << " ";
    std::cout << std::endl << std::endl;
    
    quick_sort(array, 0, size - 1);
    
    for(int p = 0; p < size; p++)
        std::cout << array[p] << " ";
    std::cout << std::endl;

    return 0;
}