/* Поразрядная сортировка O(n) */
#include <iostream>

using namespace std;

void radix_sort(int* array, int n)
{
    int digit = 0, radix = 10, max_value;
    /////////////////////////////////   Поиск максимального элемента
    for(int i = 0; i < n; i++)
        if(array[i] > max_value)
            max_value = array[i];
    /////////////////////////////////   Определение кол-ва разрядов числа 
    while(max_value != 0)
    {
        max_value = max_value/10;
        digit++;
    }
    /////////////////////////////////
}

int main()
{
    const int size = 8;
    int array[size] = {130, 42, 513, 31, 900, 2, 71, 515};
    radix_sort(array, size);
    for(int p = 0; p < size; p++)
    {
        cout << array[p] << " ";
    }
    cout << endl;

    return 0;
}