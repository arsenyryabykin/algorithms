/* Сортировка выбором, O(n^2) */

#include <iostream>
#include <vector>
#include <algorithm>

void selection_sort(int* array, int n)
{
    int smallest_index; // Переменная для индекса наименьшего эл-та
    // Проход по массиву до предпоследнего эл-та, последний эл-т уже окажется на правильном месте
    for(int i = 0; i < n - 1; i++)
    {
        // Поиска элемента в подмассиве от i-го эл-та до последнего
        smallest_index = i;
        for(int j = i; j < n; j++)
        {
            if(array[j] < array[smallest_index])
                smallest_index = j;
        }
        // Когда наименьший эл-т найден, меняем местами с первым эл-том в подмассиве, уменьшаем подмассив;
        if(smallest_index != i)
            std::swap(array[smallest_index], array[i]);
    }
}

int main()
{
    const int size = 10;
    int array[size] = {3, 0, 6, 12, 1, 11, 9, 11, 0, 3};

    for(int p = 0; p < size; p++)
        std::cout << array[p] << " ";
    std::cout << std::endl << std::endl;
    
    selection_sort(array, size);
    
    for(int p = 0; p < size; p++)
        std::cout << array[p] << " ";
    std::cout << std::endl;

    return 0;
}
